$(document).ready(function(){
   
  $('#login_user_options').click(function(){
    $("#show_user_dialog").show();
    $('.banner_con').css({'opacity':'.5'});
    $('.container').css({'opacity':'.5'});
  });

  $("#close_model").click(function(){
    $("#show_user_dialog").hide();
    $('.banner_con').css({'opacity':'1'});
    $('.container').css({'opacity':'1'});
  });

  $('#new_event').validate({
      rules: {
          "event[name]": {
              required: true
          },
          "event[long_description]": {
              required: true
          },
          "event[address]": {
              required: true
          },
          "event[category_id]": {
              required: true
          }

      }

  });

});
