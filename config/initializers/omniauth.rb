Rails.application.config.middleware.use OmniAuth::Builder do
  # The following is for facebook
  provider :facebook, EventsEnv::FACEBOOK_APP_ID, EventsEnv::FACEBOOK_SECRET_KEY, {:scope => 'email, read_stream, read_friendlists, friends_likes, friends_status'}
 
  # If you want to also configure for additional login services, they would be configured here.
end


# Rails.application.config.middleware.use OmniAuth::Builder do
#   provider :facebook, ENV['FACEBOOK_KEY'], ENV['FACEBOOK_SECRET'],
#            :scope => 'email,user_birthday,read_stream, read_friendlists, friends_likes, friends_status', :display => 'popup'
# end

# Rails.application.config.middleware.use OmniAuth::Builder do
#   provider :facebook, ENV['502839126478852'], ENV['95b9557dea95fd003549ff0761cf360b'],
#            :scope => 'email, user_birthday, read_stream, read_friendlists, friends_likes, friends_status', :display => 'popup'
# end