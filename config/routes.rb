Eventsroundme::Application.routes.draw do
  get "normal_user/dashboard" =>"normal_user#dashboard"
  match "normal_user/add_event_normal_user" =>"normal_user#add_event_normal_user"
  match "normal_user/events_attending" =>"normal_user#events_attending"
  match "normal_user/find_friends" =>"normal_user#find_friends"
  match "normal_user/category_order" =>"normal_user#category_order"
  match "normal_user/create_event" =>"normal_user#create_event"
  match "normal_user/thank_you_after_payment" =>"normal_user#thank_you_after_payment"
  match "normal_user/paypal_notification" =>"normal_user#paypal_notification"
  match "normal_user/edit_event_by_normal_user/:id" =>"normal_user#edit_event_by_normal_user"
  match "normal_user/delete_user_event/:id" =>"normal_user#delete_user_event"
 
  root :to => "events#index"
  
  match "/contact"  => "contacts#new", as: :contact_us
  match "/test"   => "static#test"
  match '/auth/:provider/callback' => 'authentications#create'

  resources :contacts

  devise_for :admin_users, ActiveAdmin::Devise.config


  resources :searches

  get "events-in/(:name)/(:id)" => "searches#events_search", as: :events_search

  devise_for :users do
    get '/users/sign_out' => 'devise/sessions#destroy'
  end
 
  get "events/city/:name" => "searches#events_by_city"

  get "popup/events_around_me"
  get "popup/homepage"
  get "popup/signup"

  get "/fetch_event" => "events#fetch_event", :via => ["options", "get", "post"]
  get "/fetch_all_city" => "events#fetch_all_city", :via => ["options", "get", "post"]

  # get "/:action"    => "static#:action"

  resources :categories
  resources :events 
  ActiveAdmin.routes(self)  
end

