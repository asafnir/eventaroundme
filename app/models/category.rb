class Category < ActiveRecord::Base
  attr_accessible :name, :shortname, :meetup_category_id
  def category_short_name
    "#{self.shortname}"
end	
end
