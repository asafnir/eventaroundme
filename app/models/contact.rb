class Contact < ActiveRecord::Base
  attr_accessible :email, :first_name, :last_name, :message, :subject
  validates_presence_of :email, :first_name, :message
end
