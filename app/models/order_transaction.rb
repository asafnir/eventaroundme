class OrderTransaction < ActiveRecord::Base
  attr_accessible :event_id,:user_id, :price, :status
  # belongs_to :event
  belongs_to :user

 def paypal_url(return_url,notify_url,event_id,user_id)
      values = {
        :business => "deepakbusiness@gmail.com", #EventsEnv::PAYPAL_BUSINESS_ID,
        :cmd => "_xclick",
        :upload => 1,
        :return => return_url,
        :invoice => "#{user_id}_#{event_id}_completed_20",
        :amount=>20,
        :item_name=>"event",
        :currency_code=>"USD",
        :notify_url=>notify_url,
      }
      # EventsEnv::PAYPAL_REDIRECT_URL + values.to_query
      "https://www.sandbox.paypal.com/cgi-bin/webscr?" + values.to_query
 end

 

end
