class AppVar < ActiveRecord::Base
  attr_accessible :key, :property, :value

  def self.contact_form_select_input_data
    where(property: "contact_form_select_input").map{|e|  [e.value, e.key]}
  end


end
