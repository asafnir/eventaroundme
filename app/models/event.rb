class Event < ActiveRecord::Base
  attr_accessible  :source, :url,
    :address, :city, :state, :country, :lat, :lon,
    :starting_at, :ending_at, :time_zone,
  	:name, :title, :host, :short_description, :long_description, 
  	:registration_starts_at, :registration_ends_at, :registration_url,
    :unique_id, :from_meetup, :from_facebook, :from_eventbrite,
    :status, :yes_rsvp_count, :no_rsvp_count, :maybe_rsvp_count,
    :is_featured_event,:image_link,:category_id ,:profile_pic 
  
  has_many :order_transactions
  mount_uploader :profile_pic, ImageUploaderUploader

   
  def valid_event?(result, address, options)
    return_condition = []

    if !address.blank?
      return_condition << address.gsub(" ","").downcase.match(options[:city].gsub(" ","").downcase).nil? ? false : true
    end

    if !result["venue"].blank? and !result["venue"]["city"].blank?
      return_condition << result["venue"]["city"].gsub(" ","").downcase.match(options[:city].gsub(" ","").downcase).nil? ? false : true
    end
    
    if !result["venue"].blank? and !result["venue"]["country"].blank?
      return_condition << result["venue"]["country"].gsub(" ","").downcase.match(options[:city].gsub(" ","").downcase).nil? ? false : true
    end

    if !result["venue"].blank? and !result["venue"]["state"].blank?
      return_condition << result["venue"]["state"].gsub(" ","").downcase.match(options[:city].gsub(" ","").downcase).nil? ? false : true
    end

    if !result["venue"].blank? and !result["venue"]["region"].blank?
      return_condition << result["venue"]["region"].gsub(" ","").downcase.match(options[:city].gsub(" ","").downcase).nil? ? false : true
    end   
    
    return return_condition
  end


  def search_fb_events options = {}
    # {:start_date=>Sat, 01 Feb 2014, :end_date=>Sun, 23 Feb 2014, :city=>"delhi india", :text=>"", :site=>"All"}
    options = options.clone
    token = 'CAAHJVFuDUAQBAKkSnSozZCVJvRzQYZAmF9b9iTGWKuwttnqY80zYxf3idb980WBrWFSHpvtC70Gaag9A8FWD1ZBPql6RD5Bx7anMieJKLB8mzst5zOVkM8aEJ1rRwSxBgeoxUNgDnfDb9ie9gsgjx5fp92MV8LKgskH23sxaKylpxGthHbD'
    @graph = Koala::Facebook::API.new(token)
    search_string = "https://graph.facebook.com/search?fields=id,name,venue,start_time,end_time,location,description,picture.width(900).height(900)"
    unless options[:city].blank?
      if options[:text].blank?
        search_string += URI.encode("&q=#{options[:city]}")
      else
        search_string += URI.encode("&q=#{options[:city]}&#{options[:text]}")  
      end
    end

    search_string += URI.encode("&limit=5000")
    search_string += URI.encode("&type=event")
    search_string += "&access_token=#{token}"

    response = RestClient.get search_string   
    json_data = JSON.parse(response)["data"] unless response.blank?
    events = []

    unless json_data.blank?
      json_data.each do |result|
        event = Event.where(unique_id: result["id"]).where(from_facebook: true)
        address = (result["location"] || '') + (result["venue"].blank? ? '' : result["venue"]["street"].blank? ? '' : result["venue"]["street"])
        if event.blank?
          if true or (result["start_time"].blank? or ((Date.parse(result["start_time"]) >= options[:start_date]) and (Date.parse(result["start_time"]) <= options[:end_date])))
            if valid_event?(result, address, options).include? true
              event = Event.create(
                :unique_id        => result["id"].blank?         ? '' : result["id"],
                :name             => result["name"].blank?       ? '' : result["name"],
                :starting_at      => result["start_time"].blank? ? '' : result["start_time"],
                :ending_at        => result["end_time"].blank?   ? '' : result["end_time"],
                :address          => result["location"].blank?   ? '' : result["location"],
                :time_zone        => result["time_zone"].blank?  ? '' : result["time_zone"],
                :lat              => result["venue"].blank?      ? '' : result["venue"]["latitude"].blank? ? '' : result["venue"]["latitude"],
                :lon              => result["venue"].blank?      ? '' : result["venue"]["longitude"].blank? ? '' : result["venue"]["longitude"],
                :city             => result["venue"].blank?      ? '' : result["venue"]["city"].blank? ? '' : result["venue"]["city"],
                :state            => result["venue"].blank?      ? '' : result["venue"]["state"].blank? ? '' : result["venue"]["state"],
                :country          => result["venue"].blank?      ? '' : result["venue"]["country"].blank? ? '' : result["venue"]["country"],
                :address          => (result["location"] || '') + (result["venue"].blank? ? '' : result["venue"]["street"].blank? ? '' : result["venue"]["street"]),
                :long_description => result["description"].blank? ? '' : result["description"],
                :image_link       => result["picture"].blank? ? '1.jpeg' : (result["picture"]["data"]["url"] rescue "1.jpeg"),
                :from_facebook    => 1,
                :from_meetup      => 0,
                :from_eventbrite  => 0
              )
              events.push(event.id)
            end
          end  
        else
          if true or (result["start_time"].blank? or ((Date.parse(result["start_time"]) >= options[:start_date]) and (Date.parse(result["start_time"]) <= options[:end_date])))
            event.first.update_attributes(
              :name             => result["name"].blank?       ? '' : result["name"],
              :starting_at      => result["start_time"].blank? ? '' : result["start_time"],
              :ending_at        => result["end_time"].blank?   ? '' : result["end_time"],
              :address          => result["location"].blank?   ? '' : result["location"],
              :time_zone        => result["time_zone"].blank?  ? '' : result["time_zone"],
              :lat              => result["venue"].blank?      ? '' : result["venue"]["latitude"].blank? ? '' : result["venue"]["latitude"],
              :lon              => result["venue"].blank?      ? '' : result["venue"]["longitude"].blank? ? '' : result["venue"]["longitude"],
              :city             => result["venue"].blank?      ? '' : result["venue"]["city"].blank? ? '' : result["venue"]["city"],
              :state            => result["venue"].blank?      ? '' : result["venue"]["state"].blank? ? '' : result["venue"]["state"],
              :country          => result["venue"].blank?      ? '' : result["venue"]["country"].blank? ? '' : result["venue"]["country"],
              :address          => (result["location"] || '') + (result["venue"].blank? ? '' : result["venue"]["street"].blank? ? '' : result["venue"]["street"]),
              :long_description => result["description"].blank? ? '' : result["description"],
              :image_link => result["picture"].blank? ? '1.jpeg' : (result["picture"]["data"]["url"] rescue "1.jpeg")
            )
            events.push(event.first.id)
          end  
        end
      end
    end       
    events
  end

  def search_meetup_events options = {}
    options = options.clone
    search_string = "https://api.meetup.com/2/open_events?&key=#{MEETUP_API_KEY}&page=5000"
    unless options[:city].blank?
      search_string += URI.encode("&city=#{options[:city]}")
    end
    unless options[:state].blank?
      search_string += URI.encode("&state=#{options[:state]}")
    end
    unless options[:country].blank?
      search_string += URI.encode("&country=#{options[:country]}")
    end
    unless options[:lat].blank? and options[:lon].blank?
      search_string += "&lat=#{options[:lat]}&lon=#{options[:lon]}"
    end
    unless options[:category].blank?
      search_string += URI.encode("&category=#{options[:category]}")
    end

    unless options[:starting_date].blank?

    end
    unless options[:ending_date].blank?

    end

    unless options[:text].blank?
      search_string += URI.encode("&text=#{options[:text]}")
    else
      search_string += URI.encode("&text=#{options[:city]}")
    end

    response = RestClient.get search_string rescue ""
    events = []

    json_data = response.nil? ? [] : JSON.parse(response.body.force_encoding("ISO-8859-1").encode("utf-8", replace: nil))
    # json_data = JSON.parse(response.body.encode('UTF-8', 'binary', invalid: :replace, undef: :replace, replace: '', nil: :replace)) unless response.blank? 
    unless json_data.blank?
     unless json_data["results"].blank?
      json_data["results"].each do |result|
        event = Event.where(unique_id: result["id"].to_s).where(from_meetup: true)
        begin
          xml = Nokogiri::XML(result["description"])
          @image = xml.xpath("//img").first.get_attribute('src')
        rescue
          @image = "1.jpeg"
        end  
        address = result["venue"].blank? ? '' : result["venue"]["address_1"].blank? ? ""  : result["venue"]["address_1"]
        if event.blank?
          if true or (result["time"].blank? or ((Time.at(result["time"].to_i / 1000).to_date >= options[:start_date].to_date) and (Time.at(result["time"].to_i / 1000).to_date <= options[:end_date])))
            if valid_event?(result, address, options).include? true
              event = Event.create(
                :source                 => "",
                :url                    => result["event_url"].blank?          ? ""  : result["event_url"],
                :address                => result["venue"].blank?              ? '' : result["venue"]["address_1"].blank? ? ""  : result["venue"]["address_1"] ,
                :city                   => result["venue"].blank?              ? '' : result["venue"]["city"].blank?      ? ""  : result["venue"]["city"],
                :state                  => result["venue"].blank?              ? '' : result["venue"]["state"].blank?     ? ""  : result["venue"]["state"],
                :country                => result["venue"].blank?              ? '' : result["venue"]["country"].blank?   ? ""  : result["venue"]["country"],
                :lat                    => result["venue"].blank?              ? '' : result["venue"]["lat"].blank?       ? ""  : result["venue"]["lat"],
                :lon                    => result["venue"].blank?              ? '' : result["venue"]["lon"].blank?       ? ""  : result["venue"]["lon"],
                :starting_at            => result["time"].blank?               ? nil : Time.at(result["time"].to_i / 1000),
                :ending_at              => nil,
                :time_zone              => nil,
                :name                   => result["name"].blank?               ? "" : result["name"],
                :title                  => nil,
                :host                   => nil,
                :short_description      => nil,
                :long_description       => result["description"].blank?        ? "" : result["description"],
                :registration_starts_at => nil ,
                :registration_ends_at   => nil ,
                :registration_url       => nil ,
                :unique_id              => result["id"],
                :image_link              => @image,
                :from_meetup            => 1,
                :from_facebook          => 0,
                :from_eventbrite        => 0,
                :status                 => result["status"].blank?             ? "" : result["status"],
                :yes_rsvp_count         => result["yes_rsvp_count"].blank?     ? 0  : result["yes_rsvp_count"],
                :maybe_rsvp_count       => result["maybe_rsvp_count"].blank?   ? 0  : result["maybe_rsvp_count"]
              )
              events.push (event.id)
            end
          end    
        else
          if true or (result["time"].blank? or ((Time.at(result["time"].to_i / 1000).to_date >= options[:start_date]) and (Time.at(result["time"].to_i / 1000).to_date <= options[:end_date])))
            event.first.update_attributes(
              :source                 => "",
              :url                    => result["event_url"].blank?          ? ""  : result["event_url"],
              :address                => result["venue"].blank?              ? '' : result["venue"]["address_1"].blank? ? ""  : result["venue"]["address_1"] ,
              :city                   => result["venue"].blank?              ? '' : result["venue"]["city"].blank?      ? ""  : result["venue"]["city"],
              :state                  => result["venue"].blank?              ? '' : result["venue"]["state"].blank?     ? ""  : result["venue"]["state"],
              :country                => result["venue"].blank?              ? '' : result["venue"]["country"].blank?   ? ""  : result["venue"]["country"],
              :lat                    => result["venue"].blank?              ? '' : result["venue"]["lat"].blank?       ? ""  : result["venue"]["lat"],
              :lon                    => result["venue"].blank?              ? '' : result["venue"]["lon"].blank?       ? ""  : result["venue"]["lon"],
              :starting_at            => result["time"].blank?               ? nil : Time.at(result["time"].to_i / 1000),
              :ending_at              => nil,
              :time_zone              => nil,
              :name                   => result["name"].blank?               ? "" : result["name"],
              :title                  => nil,
              :host                   => nil,
              :short_description      => nil,
              :long_description       => result["description"].blank?        ? "" : result["description"],
              :registration_starts_at => nil ,
              :registration_ends_at   => nil ,
              :registration_url       => nil ,
              :unique_id              => result["id"],
              :image_link             => @image,
              :from_meetup            => 1,
              :from_facebook          => 0,
              :from_eventbrite        => 0,
              :status                 => result["status"].blank?             ? "" : result["status"],
              :yes_rsvp_count         => result["yes_rsvp_count"].blank?     ? 0  : result["yes_rsvp_count"],
              :maybe_rsvp_count       => result["maybe_rsvp_count"].blank?   ? 0  : result["maybe_rsvp_count"]
            )
            events.push (event.first.id)
          end  
        end
      end
    end
  end
    events
  end

  def search_eventbrite_events options = {}
    options = options.clone
    search_string = "https://www.eventbrite.com/json/event_search?app_key=#{EVENTBRITE_API_KEY}&max=5000"
    unless options[:city].blank?
      search_string += URI.encode("&city=#{options[:city]}")
    end
    unless options[:state].blank?
      search_string += URI.encode("&region=#{options[:state]}")
    end
    unless options[:country].blank?
      search_string += URI.encode("&country=#{options[:country]}")
    end
    unless options[:lat].blank? and options[:lon].blank?
      search_string += "&latitude=#{options[:lat]}&longitude=#{options[:lon]}"
    end
    unless options[:category].blank?
      search_string += URI.encode("&category=#{options[:category]}")
    end

    unless options[:starting_date].blank?

    end
    unless options[:ending_date].blank?

    end

    unless options[:text].blank?
      search_string += "&keyword=#{options[:text]}"
    end
    
    response = RestClient.get search_string 
    events = []
    json_data = response.nil? ? [] : JSON.parse(response.body.encode('UTF-8', 'binary', invalid: :replace, undef: :replace, replace: '', nil: :replace))
    unless json_data["events"].blank?
      json_data["events"].each_with_index do |result, index|
        unless index == 0
          event = Event.where(unique_id: result["event"]["id"].to_s).where(from_eventbrite: true)
          address = result["event"]["venue"].blank? ? "" : result["event"]["venue"]["address"].blank? ? "" : result["event"]["venue"]["address"]
          if event.blank?
            if true or (result["event"]["start_date"].blank? or ((Date.parse(result["event"]["start_date"]) >= options[:start_date]) and (Date.parse(result["event"]["start_date"]) <= options[:end_date])))  
              if valid_event?(result, address, options).include? true
                event = Event.create(
                  :source                 => nil,
                  :url                    => result["event"]["url"].blank?             ? "" : result["event"]["url"],
                  :address                => result["event"]["venue"].blank?           ? "" : result["event"]["venue"]["address"].blank?   ? "" : result["event"]["venue"]["address"] ,
                  :city                   => result["event"]["venue"].blank?           ? "" : result["event"]["venue"]["city"].blank?      ? "" : result["event"]["venue"]["city"] ,
                  :state                  => result["event"]["venue"].blank?           ? "" : result["event"]["venue"]["region"].blank?    ? "" : result["event"]["venue"]["region"] ,
                  :country                => result["event"]["venue"].blank?           ? "" : result["event"]["venue"]["country"].blank?   ? "" : result["event"]["venue"]["country"] ,
                  :lat                    => result["event"]["venue"].blank?           ? "" : result["event"]["venue"]["latitude"].blank?  ? "" : result["event"]["venue"]["latitude"] ,
                  :lon                    => result["event"]["venue"].blank?           ? "" : result["event"]["venue"]["longitude"].blank? ? "" : result["event"]["venue"]["longitude"] ,
                  :starting_at            => result["event"]["start_date"].blank?      ? "" : result["event"]["start_date"],
                  :ending_at              => result["event"]["end_date"].blank?        ? "" : result["event"]["end_date"],
                  :time_zone              => result["event"]["timezone_offset"].blank? ? "" : result["event"]["timezone_offset"],
                  :name                   => nil,
                  :title                  => result["event"]["title"].blank?           ? "" : result["event"]["title"],
                  :host                   => nil,
                  :short_description      => nil,
                  :long_description       => result["event"]["description"].blank?     ? "" : result["event"]["description"],
                  :registration_starts_at => nil,
                  :registration_ends_at   => nil,
                  :registration_url       => nil,
                  :unique_id              => result["event"]["id"].blank?              ? "" : result["event"]["id"],
                  :from_meetup            => 0,
                  :from_facebook          => 0,
                  :from_eventbrite        => 1,
                  :status                 => result["event"]["status"].blank?          ? "" : result["event"]["status"],
                  :yes_rsvp_count         => nil,
                  :no_rsvp_count          => nil,
                  :maybe_rsvp_count       => nil,
                  :image_link             => result["event"]["logo"].blank?             ? "1.jpeg" : result["event"]["logo"]
                )
                events.push(event.id)
              end  
            end  
          else
            if true or (result["event"]["start_date"].blank? or ((Date.parse(result["event"]["start_date"]) >= options[:start_date]) and (Date.parse(result["event"]["start_date"]) <= options[:end_date])))
              event.first.update_attributes(
                :source                 => nil,
                :url                    => result["event"]["url"].blank?             ? "" : result["event"]["url"],
                :address                => result["event"]["venue"].blank?           ? "" : result["event"]["venue"]["address"].blank?   ? "" : result["event"]["venue"]["address"] ,
                :city                   => result["event"]["venue"].blank?           ? "" : result["event"]["venue"]["city"].blank?      ? "" : result["event"]["venue"]["city"] ,
                :state                  => result["event"]["venue"].blank?           ? "" : result["event"]["venue"]["region"].blank?    ? "" : result["event"]["venue"]["region"] ,
                :country                => result["event"]["venue"].blank?           ? "" : result["event"]["venue"]["country"].blank?   ? "" : result["event"]["venue"]["country"] ,
                :lat                    => result["event"]["venue"].blank?           ? "" : result["event"]["venue"]["latitude"].blank?  ? "" : result["event"]["venue"]["latitude"] ,
                :lon                    => result["event"]["venue"].blank?           ? "" : result["event"]["venue"]["longitude"].blank? ? "" : result["event"]["venue"]["longitude"] ,
                :starting_at            => result["event"]["start_date"].blank?      ? "" : result["event"]["start_date"],
                :ending_at              => result["event"]["end_date"].blank?        ? "" : result["event"]["end_date"],
                :time_zone              => result["event"]["timezone_offset"].blank? ? "" : result["event"]["timezone_offset"],
                :name                   => nil,
                :title                  => result["event"]["title"].blank?           ? "" : result["event"]["title"],
                :host                   => nil,
                :short_description      => nil,
                :long_description       => result["event"]["description"].blank?     ? "" : result["event"]["description"],
                :registration_starts_at => nil,
                :registration_ends_at   => nil,
                :registration_url       => nil,
                :unique_id              => result["event"]["id"].blank?              ? "" : result["event"]["id"],
                :from_meetup            => 0,
                :from_facebook          => 0,
                :from_eventbrite        => 1,
                :status                 => result["event"]["status"].blank?          ? "" : result["event"]["status"],
                :yes_rsvp_count         => nil,
                :no_rsvp_count          => nil,
                :maybe_rsvp_count       => nil,
                :image_link             => result["event"]["logo"].blank?             ? "1.jpeg" : result["event"]["logo"]
              )
              events.push(event.first.id)
            end  
          end
        end
      end
    end
    events
  end

  def combined_search_results(options = {})
    options = options.clone
    sitename = options[:site]
    if sitename == "Meetup"
      meetup_events = search_meetup_events(options)
      events = meetup_events
      return events.uniq
    elsif sitename == "Facebook"
      fb_events = search_fb_events(options)
      events = fb_events
      return events.uniq
    elsif sitename == "Eventbrite"
      eventbrite_events = search_eventbrite_events(options)
      events = eventbrite_events
      return events.uniq
    else        
      meetup_events = search_meetup_events(options)
      eventbrite_events = search_eventbrite_events(options)
      fb_events = search_fb_events(options)
      events = meetup_events + fb_events + eventbrite_events
      return events.uniq
    end  
  end

  def event_name
    return title if title
    return name if name
  end

  def event_short_description
    if short_description
      return short_description
    end
    if long_description and long_description.length > 200
      return "#{long_description.first(200)}..."
    else
      return long_description
    end 
    return "No Description."
  end

  def date
    return starting_at.strftime("%m/%d/%y") if starting_at
    return "Event date not specified, please check the description"
  end


  def update_event_data
    self
  end

  def seo_city_name city
    city.capitalize.gsub(/[\s]/,'-')
  end

end









# https://graph.facebook.com/oauth/access_token?client_id=502839126478852&client_secret=95b9557dea95fd003549ff0761cf360b&grant_type=fb_exchange_token&fb_exchange_token=CAAHJVFuDUAQBAIKecrxMkteXTGE3bA4EGh3i9XAFnOpmKTySBKW3YYGuNZCQeExFU2gOvSv2NrUlUMJUXc4U8wXAeebEc4hYixHMpwKmYfPMYnzQy1meCGZB5PljOTmait6ZA8YFZCUJNEt4JpgsGAtEYu4UG3DOWBWpN13n7vPyGA2OosKR1g4KUrNYzSsZD
# CAAHJVFuDUAQBAEmrFXVxK0vh8yRuC73uU7ZBmvuwhMvJ1LFEdAc66zT3Ea0oee5Vys51FJ4UCy4wLaX2bwqA5rdE15uLWR47mlmymsfJPzKXAnpchq1VrfZBeOrJRjBqh5dZCtwZCnCI3kYZCAMWod3kDyUuLcO2FkP5cIY0To4YQow6aB27ZA
# CAAHJVFuDUAQBAKkSnSozZCVJvRzQYZAmF9b9iTGWKuwttnqY80zYxf3idb980WBrWFSHpvtC70Gaag9A8FWD1ZBPql6RD5Bx7anMieJKLB8mzst5zOVkM8aEJ1rRwSxBgeoxUNgDnfDb9ie9gsgjx5fp92MV8LKgskH23sxaKylpxGthHbD