class Search < ActiveRecord::Base
  attr_accessible :ip_address, :search_string
  serialize :search_string
  serialize :event_list
end
