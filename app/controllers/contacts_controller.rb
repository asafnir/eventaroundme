class ContactsController < InheritedResources::Base
  actions :new, :create
  def create
    create!(notice: "", alert: "") do |success, failure|
      success.html { redirect_to contact_us_path, notice: "Feedback Submitted"}
      failure.html { render :new, alert: "Feedback Couldn't be submtted. Please try again later"}
    end
  end
end
