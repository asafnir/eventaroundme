class NormalUserController < ApplicationController
  before_filter :authenticate_user! ,:except => [:paypal_notification]
  def dashboard
    @events=Event.where("from_facebook=? and from_meetup=? and from_eventbrite=?",false,false,false).order("created_at Asc")
    respond_to do |format|
      format.html
    end
  end

  def add_event_normal_user
  	@event=Event.new
   respond_to do |format|
    format.html
  end
end

def create_event

  start_date_test=DateTime.new(params[:event]["starting_at(1i)"].to_i,params[:event]["starting_at(2i)"].to_i,params[:event]["starting_at(3i)"].to_i,params[:event]["starting_at(4i)"].to_i,params[:event]["starting_at(5i)"].to_i)
  end_date_test=DateTime.new(params[:event]["ending_at(1i)"].to_i,params[:event]["ending_at(2i)"].to_i,params[:event]["ending_at(3i)"].to_i,params[:event]["ending_at(4i)"].to_i,params[:event]["ending_at(5i)"].to_i)

  if start_date_test > end_date_test
    decision =true
  else
   decision =false
   @event=Event.create(params[:event])
   @event.from_facebook=false
   @event.from_meetup=false
   @event.from_eventbrite=false  
 end  

 respond_to do |format|
  if decision==false
    if(@event.save)
      format.html { redirect_to normal_user_dashboard_path(), notice: 'Your event has been created.' }
    else
      format.html { redirect_to normal_user_add_event_normal_user_path(), notice: @user.errors.full_messages }
    end
  elsif decision==true
    format.html { redirect_to normal_user_add_event_normal_user_path(), notice: 'The end date should not be earlier than the start date' }
  end
end
end

def events_attending
end

def find_friends
end

def category_order
end

def thank_you_after_payment
 @order_transact=OrderTransaction.find_by_user_id(current_user.id)
 respond_to do |format|
  if !@order_transact.nil? and @order_transact.status=='completed'
    format.html { redirect_to normal_user_dashboard_path(), notice: 'Congratulations! Your event is featured now. ' }
  else
    format.html { redirect_to normal_user_dashboard_path(), notice: 'Sorry!There is problem in making your event featured.' }
  end
end 
end

def edit_event_by_normal_user
  @event = Event.find(params[:id])
end

def delete_user_event
  Event.find(params[:id]).destroy
  redirect_to :back
end

def paypal_notification
  p "*********************************************************************"
  p params[:invoice]
  returned_invoice=params[:invoice].split('_')
  p returned_invoice[1]
  OrderTransaction.create(:user_id=>returned_invoice[0],:event_id=>returned_invoice[1],:status=>"#{returned_invoice[2]}",:price=>returned_invoice[3])
  Event.find_by_id(returned_invoice[1]).update_attributes(:is_featured_event=>true)
  render :nothing => true
end

end
