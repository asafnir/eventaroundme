require 'open-uri'

class EventsController < ApplicationController
  before_filter :set_data_variable
  
  def set_data_variable
    @data = Hash.new
  end

  # GET /events/city/City-name
  def events_by_city
    @events = Event.where(:city => params[:city].to_s)
    
    respond_to do |format|
      format.html # events_by_city.html.haml
      format.json { render json: @events }
    end    
  end

  def fetch_event
    @event = Event.where("LOWER(city) LIKE ? and starting_at > ?","%#{params[:city].to_s.downcase}%",Time.now)
    events= @event.to_json
    respond_to do |format|
       format.json{render :json => events, :callback => params[:callback]}
    end
  end

  def fetch_all_city
    @event = Event.select(:city).uniq
    events= @event.to_json
    respond_to do |format|
       format.json{render :json => events, :callback => params[:callback]}
    end
  end

  # GET /events
  # GET /events.json
  def index
    # Event.where("starting_at < '#{Time.now.to_date}'").destroy_all
    
    data = serach_local_events(request);
    unless data.nil?
      @events_array = []
      @data[:events].each do |event|
      

        @match_city = 0
        
        final_address_string = (event.address + event.city + event.country + event.state).split(/\s+(?=(?:[^"]*"[^"]*")*[^"]*$)/);
        search_string = @city.gsub(/\s+/, ' ').split(" ");

        search_string.each do |f|
          final_address_string.each do |ff|
            unless ff.downcase.match(f.downcase).nil?
              @match_city = @match_city+1
            end
          end          
        end

        unless event.starting_at.blank?
          if event.starting_at.to_date >= Time.now.to_date and @match_city == search_string.count
            @events_array << event
          end
        else
          if @match_city == search_string.count
            @events_array << event
          end
        end  
        
      end
      @data[:events] = @events_array
      @data[:events] = @data[:events].sort { |x,y| y.starting_at.to_date <=> x.starting_at.to_date }
      @data[:events] = @data[:events].reverse
    end  


    unless data.nil?
      if params[:page].nil?
        @data[:events] = @data[:events].first(9);
      else
        limit = params[:page].to_i*9
        @data[:events] = @data[:events].drop(limit-9).first(9);
      end      
    end
    @data[:events] = Event.last(9) unless data
    @search = Search.new
    
    respond_to do |format|
      format.html # index.html.haml
      # format.json { render json: @events }
      format.json { render :json => {:html => render_to_string(:partial => "event.html.haml")}.to_json }
    end
  end

  # GET /events/1
  # GET /events/1.json
  def show
    @event = Event.find(params[:id])
    if @event.blank?
      redirect_to :back and return
    else
      @event.update_event_data
    end
    
    location = @event.city unless @event.city.blank?
    location = @event.country if @event.city.blank?    

    @cal_url = generate_calendar_button(@event.event_name, @event.event_name, @event.starting_at, @event.ending_at, location,"http://nir-event.herokuapp.com", "Nir Event")
    respond_to do |format|
      format.html # show.html.haml
      format.json { render json: @event }
    end
  end

  # GET /events/new
  # GET /events/new.json
  def new
    @event = Event.new

    respond_to do |format|
      format.html # new.html.haml
      format.json { render json: @event }
    end
  end

  # GET /events/1/edit
  def edit
    @event = Event.find(params[:id])
  end

  # POST /events
  # POST /events.json
  def create
    @event = Event.new(params[:event])

    respond_to do |format|
      if @event.save
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.json { render json: @event, status: :created, location: @event }
      else
        format.html { render action: "new" }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /events/1
  # PUT /events/1.json
  def update
    @event = Event.find(params[:id])

    respond_to do |format|
      if @event.update_attributes(params[:event])
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event = Event.find(params[:id])
    @event.destroy

    respond_to do |format|
      format.html { redirect_to events_url }
      format.json { head :no_content }
    end
  end

  def search
    if request.post?
      search = Search.new(
        :search_string => params[:search],
        :ip_address    => request.remote_ip
      )
      # search_results = Event.
    end

    respond_to do |format|
      format.html
      format.json
    end
  end
end

private

def generate_calendar_button(name, description, start, end_time, location, mysite_url, mysite_name) 
   google_url = 'http://www.google.com/calendar/event?action=TEMPLATE'
   google_url += URI.encode("&text=#{name}")
   if !start.blank? and !end_time.blank?
      google_url += URI.encode("&dates=#{start.strftime('%Y%m%dT%H%M')}00/#{end_time.strftime('%Y%m%dT%H%M')}00")      
   end    
   google_url += URI.encode("&details= For details visit: #{mysite_url}")
   google_url += URI.encode("&location=#{location}")
   google_url += URI.encode("&sprop=#{mysite_url}")
   google_url += URI.encode("&sprop=name:#{mysite_name}")
   return google_url
end

def serach_local_events(request)
  # return nil if request.location.nil?
  # @city = request.location.data["city"].blank? ? request.location.data["country_name"] : request.location.data["city"] 
  # return nil if @city.blank?
  # @city.downcase.gsub("-"," ").gsub(","," ").gsub(/\s+/, ' ');
  
  @city = locateAddressByIp(request.location.data["ip"])
  @search_string = {
      start_date: '',
      end_date: '',
      city: @city,
      text: '',
      site: 'All'
    }
    

    @search = Search.where(search_string: @search_string.to_yaml)
     if @search.blank? 
      @search = Search.new(
        :search_string => @search_string,
        :ip_address => "" # params[:search][:ip_address]
      )
      @search.save
     else
        @search = @search.first
     end

    # if true # or @search.event_list.blank?
    event_list = Event.new.combined_search_results(@search.search_string)
    @search.update_attribute :event_list, event_list
    # end

    event_list = event_list.blank? ? @search.event_list : event_list

    if event_list.blank?
      @data[:events] = []
    elsif event_list.count == 1
      @data[:events] = Event.where("id in (#{event_list.join})")
    elsif event_list.count > 1
      @data[:events] = Event.where("id in (#{event_list.join(',')}) AND starting_at > '#{Time.now.to_date}' AND (ending_at > '#{Time.now.to_date}' OR ending_at IS NULL)")
    end
end

def locateAddressByIp(ip)
  doc = Nokogiri::HTML(open("http://api.ipinfodb.com/v3/ip-city/?key=#{ENV['IP_INFO_DB_API_KEY']}&ip=#{ip}"))
  city = doc.text.split(';')[6].try(:strip).try(:downcase)
  city
end