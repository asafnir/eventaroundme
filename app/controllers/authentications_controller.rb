class AuthenticationsController < ApplicationController
# POST /authentications
  # POST /authentications.json
  def create
    omniauth = request.env["omniauth.auth"]

    #render :text => request.env["omniauth.auth"].to_yaml
    authentication = Authentication.find_by_provider_and_uid(omniauth['provider'], omniauth['uid'])

    if authentication
      flash[:notice] = "Signed in successfully."
      sign_in_and_redirect(:user, authentication.user)
    elsif current_user
      current_user.apply_omniauth(omniauth)
      flash[:notice] = "Authentication successful."
      redirect_to root_url
    else
      user_exist = User.find_by_email(omniauth[:info][:email])
      if user_exist.present?
        user_exist.apply_omniauth(omniauth)
        user_exist.save
        flash[:notice] = "Signed in successfully."
        sign_in_and_redirect(:user, user_exist)
      else
        user = User.new
        user.apply_omniauth(omniauth)
        # user.confirm!
        user.save
        @user_new=User.find_by_email(user.email)
        flash[:notice] = "Signed in successfully."
        sign_in_and_redirect(:user,user)
      end
    end
  end
end

