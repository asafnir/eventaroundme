class SearchesController < ApplicationController
  before_filter :set_data_variable

  def set_data_variable
    @data = Hash.new
  end

  def events_search
    # {"name"=>"Devere-hotel", "id"=>"170"}
    # {"name"=>"Delhi-india", "id"=>"64"}

    @search_name = params[:name]
    @search_id = params[:id]
    @search = Search.find(params[:id])
    if @search.event_list.blank?
      event_list = Event.new.combined_search_results(@search.search_string)
      @search.update_attribute :event_list, event_list
    end
    event_list = event_list.blank? ? @search.event_list : event_list

    if event_list.blank?
      @data[:events] = []
    elsif event_list.count == 1
      @data[:events] = Event.where("id in (#{event_list.join})")
    elsif event_list.count > 1
      @data[:events] = Event.where("id in (#{event_list.join(',')}) AND starting_at > '#{Time.now.to_date}'")
    end

    unless @data[:events].nil?
      @events_array = []
      @data[:events].each do |event|
        @match_city = 0
        
        final_address_string = (event.address + event.city + event.country + event.state).split(/\s+(?=(?:[^"]*"[^"]*")*[^"]*$)/);
        search_string = @search.search_string[:city].gsub(/\s+/, ' ').split(" ");

        search_string.each do |f|
          final_address_string.each do |ff|
            unless ff.downcase.match(f.downcase).nil?
              @match_city = @match_city+1
            end
          end          
        end

        unless @search.search_string[:start_date].blank?
          if event.starting_at.to_date >= @search.search_string[:start_date].to_date and @match_city == search_string.count
            @events_array << event
          end
        else
          if @match_city == search_string.count
            @events_array << event
          end
        end  
        
      end
      @data[:events] = @events_array
      @data[:events] = @data[:events].sort { |x,y| y.starting_at.to_date <=> x.starting_at.to_date }
      @data[:events] = @data[:events].reverse
    end  

    unless @data[:events].nil?
      if params[:page].nil?
        @data[:events] = @data[:events].first(9);
      else
        limit = params[:page].to_i*9
        @data[:events] = @data[:events].drop(limit-9).first(9);
      end      
    end

    respond_to do |format|
      format.html { render :show }
      #format.json { render json: @search }
      format.json { render :json => {:html => render_to_string(:partial => "event.html.haml")}.to_json }
    end
  end

  # GET /searches
  # GET /searches.json
  def index
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @searches }
    end
  end

  # GET /searches/1
  # GET /searches/1.json
  def show
    @search = Search.find(params[:id])
    unless @search.event_list.blank?
      event_list = Event.new.combined_search_results(@search.search_string)
    end

    @search.update_attribute :event_list, event_list

    @data[:events] = Event.find(event_list).sort_by(&:starting_at)

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @search }
    end
  end

  # GET /searches/new
  # GET /searches/new.json
  def new
    @search = Search.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @search }
    end
  end

  # GET /searches/1/edit
  def edit
    @search = Search.find(params[:id])
  end

  # POST /searches
  # POST /searches.json
  def create
    # "start_date"=>"30-11-2013", "end_date"=>"24-01-2014",

    params[:location] = params[:location].downcase.gsub("-"," ").gsub(","," ").gsub(/\s+/, ' ');

    # location_text = params[:location]
    # unless location_text.split(",").blank?
    #   unless location_text.split(",")[0].blank?
    #     params[:location] = location_text.split(",")[0]
    #   end
    # end
    
    @search_string = {
      start_date: (Date.parse(params[:start_date]) rescue ''),
      end_date: (Date.parse(params[:end_date]) rescue ''),
      city: (params[:location] rescue ''),
      text: (params[:search_string] rescue ''),
      site: (params[:select_site] rescue '')
    }

    @search = Search.where(search_string: @search_string.to_yaml)
    
    if @search.blank? || (@search.first.created_at < (Date.today - 1.week)) 
      @search = Search.new(
        :search_string => @search_string,
        :ip_address => params[:search][:ip_address]
      )
      respond_to do |format|
        if @search.save
          format.html { redirect_to events_search_path(Event.new.seo_city_name(@search.search_string[:city]),@search.id)} 
        else
          format.html { redirect_to :back, alert: "Some error has occured"}
        end
      end
    else
      respond_to do |format|
        format.html { redirect_to events_search_path(Event.new.seo_city_name(@search.first.search_string[:city]),@search.first.id)}
      end
    end
  end

  # PUT /searches/1
  # PUT /searches/1.json
  def update
    @search = Search.find(params[:id])

    respond_to do |format|
      if @search.update_attributes(params[:search])
        format.html { redirect_to @search, notice: 'Search was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @search.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /searches/1
  # DELETE /searches/1.json
  def destroy
    @search = Search.find(params[:id])
    @search.destroy

    respond_to do |format|
      format.html { redirect_to searches_url }
      format.json { head :no_content }
    end
  end

  def events_by_city
    city = params[:name]
    options = {city: city,text: "event"}
    @events = Event.new.combined_search_results(options)
    respond_to do |format|
      format.html
      format.json
    end
  end

end
