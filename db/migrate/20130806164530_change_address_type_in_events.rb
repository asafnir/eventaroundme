class ChangeAddressTypeInEvents < ActiveRecord::Migration
  def up
    change_column :events, :address, :text
  end

  def down
  end
end
