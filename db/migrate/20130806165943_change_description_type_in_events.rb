class ChangeDescriptionTypeInEvents < ActiveRecord::Migration
  def change
    change_column :events, :short_description, :text
    change_column :events, :long_description, :text
  end
end
