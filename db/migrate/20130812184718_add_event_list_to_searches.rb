class AddEventListToSearches < ActiveRecord::Migration
  def change
    add_column :searches, :event_list, :text
  end
end
