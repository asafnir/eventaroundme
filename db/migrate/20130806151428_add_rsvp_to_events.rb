class AddRsvpToEvents < ActiveRecord::Migration
  def change
    add_column :events, :yes_rsvp_count, :integer
    add_column :events, :no_rsvp_count, :integer
    add_column :events, :maybe_rsvp_count, :integer
  end
end
