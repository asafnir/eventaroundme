class AddIsFeaturedEventToEvents < ActiveRecord::Migration
  def change
    add_column :events, :is_featured_event, :boolean
  end
end
