class CreateSearches < ActiveRecord::Migration
  def change
    create_table :searches do |t|
      t.text :search_string
      t.string :ip_address

      t.timestamps
    end
  end
end
