class CreateAppVars < ActiveRecord::Migration
  def change
    create_table :app_vars do |t|
      t.string :property
      t.string :key
      t.string :value

      t.timestamps
    end
  end
end
