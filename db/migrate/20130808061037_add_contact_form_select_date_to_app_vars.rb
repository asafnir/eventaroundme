class AddContactFormSelectDateToAppVars < ActiveRecord::Migration
  def change
    AppVar.create(property: "contact_form_select_input", key: "na", value: "Choose one")
    AppVar.create(property: "contact_form_select_input", key: "suggestion", value: "General Suggestions")
    AppVar.create(property: "contact_form_select_input", key: "service", value: "General Customer Service")
    AppVar.create(property: "contact_form_select_input", key: "product", value: "Product Support")
  end
end
