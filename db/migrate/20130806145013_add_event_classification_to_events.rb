class AddEventClassificationToEvents < ActiveRecord::Migration
  def change
    add_column :events, :unique_id, :string
    add_column :events, :from_meetup, :boolean
    add_column :events, :from_facebook, :boolean
    add_column :events, :from_eventbrite, :boolean
  end
end
