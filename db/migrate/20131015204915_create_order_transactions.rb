class CreateOrderTransactions < ActiveRecord::Migration
  def change
    create_table :order_transactions do |t|
      t.integer :event_id
      t.integer :user_id
      t.decimal :price
      t.string :status

      t.timestamps
    end
  end
end
