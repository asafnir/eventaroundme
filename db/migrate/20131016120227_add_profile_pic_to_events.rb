class AddProfilePicToEvents < ActiveRecord::Migration
  def change
    add_column :events, :profile_pic, :string
  end
end
